// Le istruzioni verranno rappresentate da 5 cifre esadecimali
// I comandi verranno posti all inizio dell istruzione e rappresentati con 2 cifre esadecimali
// Le locazioni verranno poste alla fine dell´ istruzione e rappresentate con 3 cifre esadecimali


//============= LEGENDA COMANDI =============
#define LEGGI_DEC                        0x10  
#define STAMPA_DEC                       0x11
#define ACAPO                            0x12
#define LEGGI_STR                        0x13
#define STAMPA_STR                       0x14
#define LEGGI_FLOAT                      0x15
#define STAMPA_FLOAT                     0x16
		                        
#define CARICA                           0x20 
#define IMMAGAZZINA                      0x21
#define CARICA_FLOAT                     0x22 
#define IMMAGAZZINA_FLOAT                0x23 
		                        
#define SOMMA                            0x30  
#define SOTTRAI                          0x31
#define DIVIDI                           0x32 
#define MOLTIPLICA                       0x33
#define RESTO                            0x34
#define POTENZA                          0x35
#define SOMMA_FLOAT                      0x36  
#define SOTTRAI_FLOAT                    0x37
#define DIVIDI_FLOAT                     0x38 
#define MOLTIPLICA_FLOAT                 0x39
#define POTENZA_FLOAT                    0x3A
				        
#define SALTA                            0x40  
#define SALTA_SE_NEG                     0x41
#define SALTA_SE_ZERO                    0x42 
#define STOP                             0x43 
//===========================================

#define MEMORIA 1000 //non e'possibile modificare questo numero (1000 loc. rappresentabili con esattamente tre cifre)
#define MAX_STR 50 //e' possibile modificarlo a piacimento

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int memoria[MEMORIA]={0};

int main(){
  //inizializzazioni
  int accumulatore=0,registro=0,operando=0,comando=0,cursore=0;
  float accumulatore_float=0,tmp;
  //c contatore di fine memoria, a variabile controllo while e corretta immissione programma
  int immissione,a=0,c=0,shell=1; 
  char nomeProgramma[20];
  FILE *programmaPtr;
	
  //prototipi di funzione
  void dump_SimplHEXtron(void);
  void salvaStr(char *s,int locMem,int lun);
  void stampaStr(int locMem);
  float ottieniFloat(int varloc);
  void salvaFloat(float numero,const int loc);
  
  while(shell){
    
    //Caricare il programma nel Simpletron
    printf("utente@SimplHEXtron:$ ");
    scanf("%s%*c",nomeProgramma);//ottengo nome del file con linguaggio macchina

    //uscire dal SimplHEXtron
    if(!strcmp("esci",nomeProgramma))
      shell=0;
    //eseguire un dump
    else if(!strcmp("dump",nomeProgramma))
      dump_SimplHEXtron();

    //se esiste il programma cercato
    else{
      if((programmaPtr=fopen(nomeProgramma,"r"))==NULL){
	puts("Errore: apertura programma fallito");
	a=-1;
      }
      else{
	//fintanto che non è finito lo salvo in memoria
	c=0;//il programma successivo lo ri salvo dall'inizio
	while(!a){
	  fscanf(programmaPtr,"%x",&immissione);
	  if(!feof(programmaPtr)){
	    memoria[c]=immissione;
	    c++;//vado avanti a riempire la memoria
	    if(c==MEMORIA){
	      puts("Errore: memoria SimplHEXtron esaurita");
	      dump_SimplHEXtron();
	      a=-1;//esci con errori
	    }
	  }//fine if feof
	  else
	    a=1;//esci senza errori
	}//fine while
	fclose(programmaPtr);
      }//fine else fopen

      //se ho fallito l input del programma prima esco subito
      if(a==-1)
	a=0;
      else
	printf("Caricamento terminato, Eseguo il programma\n");

      while(a){//fintanto che non arriva un comando di fine programma
	char str[MAX_STR];
	      
	//ottengo comando e operando
	registro=memoria[cursore];//parto dalla locazione puntata dal cursore
	comando=registro/(16*16*16);
	operando=registro%(16*16*16);
	//printf("registro:%x,comando:%x comendoDec:%d,operando:%x,cursore:%x\n",registro,comando,comando,operando,cursore);

	//printf("%X\n",registro);
	//eseguo comando
	switch(comando){
	  
	  //carica l´operando nell´accumulatore
	case CARICA: 
	  accumulatore=memoria[operando];
	  break;
	  
	case CARICA_FLOAT: 
	  accumulatore_float=ottieniFloat(memoria[operando]);
	  break;

	  //dividi l´accumulatore per l´operando
	case DIVIDI: 
	  if(memoria[operando]!=0)//se è una divisione possibile
	    accumulatore/=memoria[operando];
	  else{
	    printf("Errore: Tentata divisione per zero\n");
	    dump_SimplHEXtron();
	    return 1;//errore fatale a tempo di esecuzione
	  }
	  break;

	case DIVIDI_FLOAT: 
	  tmp=ottieniFloat(memoria[operando]);
	  if(tmp!=0)//se è una divisione possibile
	    accumulatore_float/=tmp;
	  else{
	    printf("Errore: Tentata divisione per zero\n");
	    dump_SimplHEXtron();
	    return 1;//errore fatale a tempo di esecuzione
	  }
	  break;

	  //salva il valore dell´accumulatore nell´operando
	case IMMAGAZZINA:
	  memoria[operando]=accumulatore;//comando di immagazzinamento
	  break;

	case IMMAGAZZINA_FLOAT:
	  salvaFloat(accumulatore_float,operando);
	  break;

	  //leggi un decimale da tastiera e immagazzinalo in operando
	case LEGGI_DEC:
	  scanf("%d%*c",&memoria[operando]);
	  break;

	  //moltiplica l´accumulatore per l´operando
	case MOLTIPLICA:
	  accumulatore*=memoria[operando];//comando di moltiplicazione
	  break;

	case MOLTIPLICA_FLOAT:
	  accumulatore_float*=ottieniFloat(memoria[operando]);//comando di moltiplicazione
	  break;

	  //goto operando
	case SALTA:
	  cursore=operando-1;//il -1 compensa l'incremento successivo
	  //printf("vai a %d\n",cursore);
	  break;

	  //goto operando se accumulatore < 0
	case SALTA_SE_NEG:
	  if(accumulatore<0)
	    cursore=operando-1;//il -1 compensa l'incremento successivo
	  break;

	  //goto operando se accumulatore == 0
	case SALTA_SE_ZERO:
	  if(accumulatore==0)
	    cursore=operando-1;//il -1 compensa l'incremento successivo
	  break;

	  //somma l´operando all´accumulatore
	case SOMMA:
	  accumulatore+=memoria[operando];
	  break;

	  //sottrai l´operando all´accumulatore
	case SOTTRAI:
	  accumulatore-=memoria[operando];
	  break;

	  //stampa a video l´operando (decimale)
	case STAMPA_DEC:
	  printf("%d",memoria[operando]);
	  break;

	  //lascia nell´accumulatore il resto di accumulatore/operando
	case RESTO:
	  if(memoria[operando]!=0)//se è una divisione possibile
	    accumulatore%=memoria[operando];
	  else{
	    printf("Errore: Tentata divisione per zero\n");
	    dump_SimplHEXtron();
	    return 1;
	  }
	  break;

	  //stampa newline
	case ACAPO:
	  putchar('\n');
	  break;

	  //eleva l´accumulatore all´operando
	case POTENZA:
	  accumulatore=(int)pow(accumulatore,memoria[operando]);
	  break;

	case POTENZA_FLOAT:
	  accumulatore_float=pow(accumulatore_float,ottieniFloat(memoria[operando]));
	  break;

	  //fine programma
	case STOP:
	  a=0;//comando di terminazione del programma
	  cursore=-1; //ci sarà un cursore++ dopo che riporterà a zero
	  break;

	  //immagazzina stringa in formato 3lun/2Ascii nell operando
	case LEGGI_STR:
	  fgets(str,MAX_STR,stdin);
	  str[strlen(str)-1]='\0';//elimino carattere newline alla fine
	  salvaStr(str,operando,strlen(str));
	  break;

	case STAMPA_STR:
	  stampaStr(operando);
	  break;

	case LEGGI_FLOAT:
	  //ottengo numero
	  scanf("%f%*c",&tmp);
	  salvaFloat(tmp,operando);
	  break;

	case STAMPA_FLOAT:
	  printf("%f",ottieniFloat(memoria[operando]));
	  break;

	case SOMMA_FLOAT:
	  accumulatore_float+=ottieniFloat(memoria[operando]);
	  break;
	
	case SOTTRAI_FLOAT:
	  accumulatore_float-=ottieniFloat(memoria[operando]);
	  break;

	default:
	  printf("Errore: \"%X-%X\" Comando non trovato\n",comando,operando);
	  dump_SimplHEXtron();
	  return 1;//errore fatale a tempo di esecuzione
	}
	cursore++;//continuo la sequenza dei comandi
      }
      if(a)
	printf("\n\nProgramma %s terminato\n",nomeProgramma);
    }//fine else
  }//fine while emulatore bash
  return 0;
}//fine main

void salvaFloat(float numero,const int loc){
  //exafloat rappresentati nel formato 1+4
  //la precisione è limitata a 4 cifre significative
  /*tabella esponenti:
    0  -> e^-8
    ...
    7  -> e^-1
    ...
    B  -> e^
    ...
    F  -> e^
  */
  int d,exp,mantissa;
  char app[5];//4 cifre più terminazione
  char exa[]="0123456789ABCDEF";

  if(numero==0)
    memoria[loc]=0;
  else{
    //ottengo esponente 
    for(d=-8;d<8;d++)//posso rappresentare 16 ordini di grandezza differenti
      if( abs(numero/pow(10,d))<10 && abs(numero/pow(10,d))>=1 ){
	exp=d-3;//la mantissa la scrivo come numero decimale di 4 cifre quindi ha due esponenti di troppo
	break;
      }//fine if
    //ottengo mantissa
    if( abs(numero)>10000 )
      while( abs(numero)>10000 )
	numero=numero/10;
    if( abs(numero)<=1000 )
      while( abs(numero)<=1000 )
	numero=numero*10;
    if(numero<0)
      numero=abs(numero)+32768;//rappresento i negativi come i numeri maggiori di 32768
    mantissa=(int)floor(numero+.5);//la rappresentazione float del C immette errore per numeri grandi
    //salvo in memoria convertendo in esadecimale e unendo mantissa e esponenziale
    sprintf(app,"%c%s%X",exa[exp+8],mantissa/4096==0?"0":"",mantissa);//4096=16*16*16 aka tre cifre esadecimali
    sscanf(app,"%X",&memoria[loc]);
  }
}

float ottieniFloat(int varloc){
  char app[5];
  float n;
  int exp,mantissa;
  if(varloc==0)
    return 0;
  sprintf(app,"%X",varloc);
  sscanf(app+1,"%X",&mantissa);
  app[1]=' ';//separa l'esponenziale dalla mantissa
  sscanf(app,"%X",&exp);
  exp=exp-8;
  if(mantissa>32768){
    mantissa-=32768;
    mantissa*=-1;
  }
  n=mantissa*pow(10,exp);
  return n;
}

void dump_SimplHEXtron(void){
  int x;
  //per ogni locazione di memoria 
  for(x=1;x<=100/*MEMORIA MOSTRATA*/;x++){ //(ne stampo solo cento per migliorare la leggibilita)
    printf("%05X\t",memoria[x-1]);
    if(x%10==0)//se sono arrivato a nove o multipli
      printf("\n");//porto a capo il cursore
  }//fine for
}

//salva ricorsivamente ogni carattere di una stringa
void salvaStr(char *s,int locMem,int lun){
  if(lun>0){
    char app[MAX_STR];
    sprintf(app,"%X%X",lun,s[0]);
    sscanf(app,"%X",&memoria[locMem]);
    salvaStr(s+1,locMem+1,lun-1);
  }
}

void stampaStr(int locMem){
  int ascii,lun;
  ascii=memoria[locMem]%(16*16);
  lun=memoria[locMem]/(16*16);
  putchar(ascii);
  if(lun>1)
    stampaStr(locMem+1);
}
